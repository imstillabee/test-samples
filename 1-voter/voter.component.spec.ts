import { VoterComponent } from './voter.component';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';

describe('VoterComponent', () => {
  let component: VoterComponent
  let fixture: ComponentFixture<VoterComponent>

  beforeEach(() => {
    TestBed.configureTestingModuleMethod({
      declarations: [ VoterComponent ]
    });
    
    // this returns a component fixture, a wrapper around your component instance
    fixture = TestBed.createComponent(VoterComponent)
    component = fixture.componentInstance;
    // fixture.nativeElement
    // fixture.debugElement
  });

  it('should render total votes', () => {
    component.othersVote = 20;
    component.myVote = 1;
    fixture.detectChanges();

    let de = fixture.debugElement.query(By.css('.vote-count'));
    let el: HTMLElement = de.nativeElement

    expect(el.innerText).toContain(21);
  });

  it('should highlight the upvoted button, if i have uploaded', () => {
    component.myVote = 1;
    fixture.detectChanges();

    let de = fixture.debugElement.query(By.css('.glyphicon-menu-up'));

    expect(de.classes['highlighted']).toBeTruthy();
  });

  it('should increase total votes when i click the vote button', () => {
    let button = fixture.debugElement.query(By.css('glyphicon-menu-up'));
    button.triggerEventHandler('click', null);

    expect(component.totalVotes).toBe(1);
  })
});
